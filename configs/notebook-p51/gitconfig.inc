[user]
	email = mail@schmidt-andreas.de
	signingkey = FEE0A611BEA6DEA0
[credential]
	helper = "!pass-git-helper -m $HOME/.config/pass-git.cfg $@"
[credential "https://mstfs.claas.com"]
	helper = cache
[sendemail]
	smtpserver = mail.schmidt-andreas.de
	smptuser = mail@schmidt-andreas.de
	smptencryption = ssl
	smptserverport = 465
[http "https://gitea.lan"]
	sslVerify = false
