alias cdeit='echo "\e]11;#002\a";docker-compose -f ~/code/docker/docker-compose.yml run --rm eit-dev;echo "\e]11;#000\a"'
alias deit='docker-compose -f ~/code/docker/docker-compose.yml run --rm eit-dev'
alias dyocto='docker-compose -f ~/code/docker/docker-compose.yml run --rm sumo-yocto'
alias ssh='TERM=xterm-256color ssh'
