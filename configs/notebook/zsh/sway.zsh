if [ -z "${DISPLAY}" ] && [ "$(tty)" = "/dev/tty1" ]; then
	exec sway -V -d 2>&1 >> /var/log/sway.log
fi
