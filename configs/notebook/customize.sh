#!/bin/bash

# Set wallpaper for lightdm
sed -i "/\[greeter\]/{n;s|^#background=.*$|background=/usr/share/archlinux/wallpaper/archlinux-aqua-vines.jpg|}" /etc/lightdm/lightdm-gtk-greeter.conf

# Prepare for using pulseaudio
execAsUser "$USER_NAME" systemctl --user mask pulseaudio.socket

# Autoenable of bluetooth
sed -i "s|^#AutoEnable.*|AutoEnable=true|" /etc/bluetooth/main.conf

# Set libinput for X11
ln -s /usr/share/X11/xorg.conf.d/40-libinput.conf /etc/X11/xorg.conf.d/
