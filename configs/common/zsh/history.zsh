HISTSIZE=5000
HISTFILE=~/.cache/zsh/history
SAVEHIST=5000
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt incappendhistory
setopt hist_ignore_space

[ ! -d ~/.cache/zsh ] && mkdir ~/.cache/zsh
