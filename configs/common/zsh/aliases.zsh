alias ls='ls --color=auto'
alias vim='nvim'

eval $(thefuck --alias)

# file types
alias -s {cpp,hpp}=vim
alias -s md=vim
alias -s pdf=evince
