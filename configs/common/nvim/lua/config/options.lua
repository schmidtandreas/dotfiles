-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

local opt = vim.opt

opt.list = false
opt.listchars = { tab = "▸~", trail = "␣", extends = ">", precedes = "<", eol = "↲" }

opt.tabstop = 4
opt.shiftwidth = 4

opt.colorcolumn = "120"

opt.wrap = false
