return {
  "neovim/nvim-lspconfig",
  servers = {
    pylsp = {
      settings = {
        pylsp = {
          plugins = {
            pycodestyle = {
              ignore = { "E501" },
              maxLineLength = 200,
            },
          },
        },
      },
    },
  },
}
