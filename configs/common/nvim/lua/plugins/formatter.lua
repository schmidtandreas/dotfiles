return {
  "stevearc/conform.nvim",
  lazy = true,
  cmd = "ConformInfo",
  opts = function()
    local opts = {
      formatters_by_ft = {
        sh = { "shfmt" },
        python = { "black" },
      },
      formatters = {
        shfmt = {
          prepend_args = { "-i", "4", "-ci", "-fn" },
        },
      },
    }
    return opts
  end,
}
