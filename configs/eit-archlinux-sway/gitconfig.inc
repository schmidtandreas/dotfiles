[user]
	email = andreas.schmidt@claas.com
	signingkey = FDACBE5F8238CB0B
[http]
	proxyAuthMethod = basic
[http "https://192.168.0.50/gitea"]
	sslVerify = "false"
[credential "https://mstfs.claas.com"]
	helper = "!pass-git-helper -m $HOME/.config/pass-git-helper/claas.cfg $@"
[credential "https://gitlab.com/schmidtandreas"]
	helper = "!pass-git-helper -m $HOME/.config/pass-git-helper/claas.cfg $@"
[credential "https://github.com/schmidtandreas"]
	helper = "!pass-git-helper -m $HOME/.config/pass-git-helper/claas.cfg $@"
[ado]
	pr-token = "gvjy3og34z3q5kk3aiexrmaco3crg5eqlgksvhyxbj6od7gvbaia"
