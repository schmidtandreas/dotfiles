alias cdeit='echo "\e]11;#002\a";docker-compose -f ~/code/docker/docker-compose.yml run --rm eit-dev;echo "\e]11;#000\a"'
alias deit='docker-compose -f ~/code/docker/docker-compose.yml run --rm eit-dev'
alias smeit='docker-compose -f ~/code/docker/docker-compose.yml run --rm eit-swmgmt'
alias dyocto-sumo='docker-compose -f ~/code/docker/docker-compose.yml run --rm sumo-yocto'
alias dyocto-honster='docker-compose -f ~/code/docker/docker-compose.yml run --rm honister-yocto'
alias ssh="TERM=xterm-256color ssh"
alias screen="/usr/bin/screen -c ~/.config/screenrc"
