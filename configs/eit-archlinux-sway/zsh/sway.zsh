if [ -z "${DISPLAY}" ] && [ "$(tty)" = "/dev/tty1" ]; then
	export WLR_NO_HARDWARE_CURSORS=1
    export XDG_CURRENT_DESKTOP=sway
	exec sway -d -V 2>&1 >>/var/log/sway.log
fi
