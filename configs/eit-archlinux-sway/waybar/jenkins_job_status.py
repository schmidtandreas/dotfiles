#!/home/andreas/.config/waybar/env/bin/python3

import logging
import jenkins
from pypass import PasswordStore, EntryType
import json
import random
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import sys
import os

logging.basicConfig(filename="/tmp/jj.log", level=logging.WARNING)

class JenkinsJobStatus:

    def __init__(self, config: str):
        self._jenkins_url = "https://dehwllcessrv01:8483"
        self._jenkins_pass_path = "Claas/defaultuser"
        self._jenkins = None
        self._output = {}
        self._config_path = os.path.dirname(__file__)
        self._config = self._load_config(config)

    def get_jobs_status(self):
        if not self._config:
            self._output["alt"] = "error"
            self._output["class"] = "bad"
            return self._output

        if not self._set_jenkins():
            self._output["alt"] = "error"
            self._output["class"] = "bad"
            return self._output

        if not self._is_jenkins_available():
            self._output["alt"] = "not_connected"
            self._output["class"] = "bad"
            return self._output

        jobs_status = "good"
        jobs_list = []
        for job in self._config["jobs"]:
            succeed, status = self._get_job_status(job)
            if not succeed:
                self._output["alt"] = "not_connected"
                self._output["class"] = "bad"
                return self._output
            job_entry = {"name": job['name'], "status": status}
            jobs_list.append(job_entry)
            if status == "bad":
                jobs_status = "bad"
            elif status == "degraded":
                if jobs_status == "good":
                    jobs_status = "degraded"
            elif status == "aborted":
                if jobs_status != "bad":
                    jobs_status = "aborted"
        
        self._output["alt"] = "status"
        self._output["class"] = jobs_status

        self._output["tooltip"] = ""
        for job in jobs_list:
            if job["status"] == "good":
                prefix = "<span foreground='#5F5' font-family='monospace'>󰗠"
            elif job["status"] == "bad":
                prefix = "<span foreground='#F55' font-family='monospace'>"
            elif job["status"] == "aborted":
                prefix = "<span foreground='#CCC' font-family='monospace'>󰍶"
            elif job["status"] == "degraded":
                prefix = "<span foreground='#FF0' font-family='monospace'>"
            elif job["status"] == "disabled":
                prefix = "<span foreground='#777' font-family='monospace'>󰚃"
            else:
                prefix = "[X]"
            self._output["tooltip"] += f"{prefix} : {job['name']}</span>"
            if job != jobs_list[-1]:
                self._output["tooltip"] += "\n"

        return self._output

    def _load_config(self, config: str):
        data = None
        try:
            with open(f"{self._config_path}/jj.conf") as cf:
                data = json.load(cf)
        except Exception as e:
            import os
            logging.error(f"Open config file failed: '{os.getcwd()}/jj.conf' {e}")
        return data[config] if data else None

    def _get_credentials(self):
        try:
            ps = PasswordStore()
        except Exception as e:
            logging.error(f"Initialization of pypass failed: {e}")
            return None, None
        credentials_list = ps.get_passwords_list()
        if self._jenkins_pass_path not in credentials_list:
            logging.error(f"No credentials for pass path \"{self._jenkins_pass_path}\" found.")
            return None, None
        user = ps.get_decrypted_password(self._jenkins_pass_path, EntryType.username)
        pw = ps.get_decrypted_password(self._jenkins_pass_path, EntryType.password)
        return user, pw

    def _set_jenkins(self):
        if self._jenkins:
            return True
        user, pw = self._get_credentials()
        if user is None or pw is None:
            return False
        try:
            self._jenkins = jenkins.Jenkins(self._jenkins_url, username=user, password=pw)
            requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
            self._jenkins._session.verify = False
        except Exception as e:
            logging.error(f"Jenkins init error {e}")
            return False
        return True

    def _is_jenkins_available(self):
        try:
            self._jenkins.get_whoami()
        except Exception as e:
            logging.error(f"Jenkins connection failed: {e}")
            return False
        return True

    def _get_job_status(self, job_data):
        try:
            job_info = self._jenkins.get_job_info(job_data["job"])
            if not job_info["buildable"]:
                return True, "disabled"
            last_buld_number = job_info["lastCompletedBuild"]["number"]
            build_info = self._jenkins.get_build_info(job_data["job"], last_buld_number)
            if build_info["result"] == "SUCCESS":
                return True, "good"
            elif build_info["result"] == "UNSTABLE":
                return True, "degraded"
            elif build_info["result"] == "ABORTED":
                return True, "aborted"
            else:
                return True, "bad"
        except Exception as e:
            logging.error(f"Get jenkins job info failed: {e}")
            return False, "bad"

if __name__ == "__main__":
    config = sys.argv[1]
    jjs = JenkinsJobStatus(config)
    print(json.dumps(jjs.get_jobs_status()))
